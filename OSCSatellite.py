'''
Copyright 2011 Jogi Hofmueller

This file is part of OSCSatellite.

OSCSatellite is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OSCSatellite is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OSCSatellite.  If not, see <http://www.gnu.org/licenses/>.

OSCSatellite

  Generate OSC messages from TLE data for satellites

  TODO

  * implement command line options (override configuration file)
  * check UDP checksum errors (found with tcpdump)
  * rewrite nicely (partly done) :)
  * document
	* fix the error that happens when the subscriber sends garbage as IP address

	  Could not connect to client (while sending to ('$1.$2.$3.$4', 61061): [Errno -5] No address associated with hostname).  Exiting.


'''

import ephem
import sys
import time
import optparse
import os
import signal
import threading
from datetime import datetime
from OSC import *
import errno

'''
check python version and adapt to 2.x or 3.x
'''
if sys.version_info[0] == 3:
  import configparser as cp
  import urllib.request as urlhandler
else:
  import ConfigParser as cp
  import urllib as urlhandler

'''
signal handlers
'''

def sigint_handler (signum, frame):
  print('Received (keyboard) interrupt (signal {0}).  Exiting.'.format (signum))
  server.stop ()
  sys.exit (0)

def sighup_handler (signum, frame):
  config.set_variables ()
  tles.load_from_file ()
  server.load_satellites ()

def sigalrm_handler (signum, frame):
  tles.get_from_network ()
  tles.load_from_file ()
  server.load_satellites ()
  signal.alarm (config.update_interval * 60 * 60)

'''
OSC message handlers

no special handlers for the moment ...
'''

'''
helper methods
'''

def dms2degdec (lonlat):
  '''
  convert a position argument from Deg:Min:Sec.Frac to
  Deg.Frac and round the result to 5 decimal points

  @param lonlat:  longitude or latitude in DD:MM:SS[.ss] to convert
  '''

  parts = lonlat.split (':')
  if len (parts) != 3:
    return 0.0
  degrees = float (parts[0])
  fraction =  (float (parts[1]) * 60.0 + float (parts[2])) / 3600
  if parts[0][0] == '-':
    return round (degrees - fraction, 5)
  return round (degrees + fraction, 5)

'''
classes
'''

class Satellite:
  '''
  one satellite
  '''

  def __init__ (self, tle, observer):
    self.observer = observer
    self.tle = tle
    self.sat = ephem.readtle (self.tle[0], self.tle[1], self.tle[2])

  def update (self):
    '''
    calculate new values
    '''

    self.sat.compute (self.observer)
    # self._point_in_space ()

  # def _point_in_space (self):
  #   '''
  #   sets parameters x, y and z (see sample config)
  #   '''

  #   # earth radius is set to one;  adjust satellite orbit
  #   radius = 1 + self.sat.elevation / 6371000.0

  #   longitude = math.radians (dms2degdec (self.sat.sublong.__str__ ()))
  #   latitude = math.radians (dms2degdec (self.sat.sublat.__str__ ()))

  #   # standard formulas
  #   self.z = math.sin (latitude) * radius
  #   self.x = math.cos (latitude) * math.cos (longitude) * radius
  #   self.y = math.cos (latitude) * math.sin (longitude) * radius

  #   return

class Server:
  '''
  the server
  '''

  def __init__ (self):
    pass

  def start (self):
    self.create_observer ()
    self.load_satellites ()
    self.osc_server = OSCServer ((config.address, config.port))
    self.osc_client = OSCMultiClient (self.osc_server)
    self.osc_server.addDefaultHandlers ()
    self.server = threading.Thread (target = self.osc_server.serve_forever)
    self.server.start ()

  def sendBundle (self, bundle):
    try:
      self.osc_client.send (bundle)
    except OSCClientError as e:
      print('Could not connect to client ({0}).  Exiting.'.format (e))
    bundle.clearData ()

  def run (self):
    '''
    go into endless serving loop
    '''

    while True:
      bundle = OSCBundle ()
      for sat in self.satellites:
        # anyone a better idea for setting high resolution time on the
        # observer object?
        self.observer.date = datetime.utcnow ().strftime ('%Y/%m/%d %H:%M:%S.%f')
        sat.update ()
        for d in config.data:
          msg = OSCMessage ()
          self.create_message (sat, msg, d[0], d[1])
          # send the bundle if it would exeed the message size
          # as defined in OSC.py
          if len (bundle.message) + len (msg) > 4096 * 8:
            self.sendBundle (bundle)
          bundle.append (msg)
        # send one bundle per satellite
        self.sendBundle (bundle)
      time.sleep (config.resolution)

  def stop (self):
    self.osc_server.close ()
    self.server.join ()
    self.osc_client.close ()

  def create_observer (self):
    self.observer = ephem.Observer ()
    self.observer.lon, self.observer.lat, self.observer.elevation = config.longitude, config.latitude, config.altitude

 
  def load_satellites (self):
    '''
    load satellite data from tles.tles and create instances of class Satellite ()
    '''

    self.satellites = []
    
    for l in tles.tles:
      if self.include_satellite (int (l[0])):
        self.satellites.append (Satellite (l[1], self.observer))

    print('{0}: Loaded {1} satellite(s)'.format (time.strftime ('%c'), len (self.satellites)))

  def include_satellite (self, catalog_number):
    '''
    decide if the satellite with catalog_number should be included or
    not (config.limit_to_satellites and config.exclude_satellites)
    '''

    if config.limit_to_satellites:
      if catalog_number in config.limit_to_satellites:
        return True
      return False
    if catalog_number in config.exclude_satellites:
      return False
    return True

  def create_message (self, sat, msg, name, addr):
    '''
    create an OSCMessage in msg with data from sat's attribut name
    and use catalog_number and addr for OSCMessage address
  
    @param sat:  reference to instance of class Satellite ()
    @param msg:  reference to instance of class OSCMessage ()
    @param name: string with name of attribute to use
    @param addr: string with name of address to use in OSCMessage
    '''

    msg.setAddress ('/satellite/{0}/{1}'.format (sat.sat.catalog_number, addr))
    if hasattr (sat, name):
      attr = getattr (sat, name)
    elif hasattr (sat.sat, name):
      attr = getattr (sat.sat, name)

    if isinstance (attr, ephem.Angle):
      val = dms2degdec (attr.__str__ ())
    elif isinstance (attr, int):
      val = int (attr)
    elif isinstance (attr, float):
      val = float (attr)
    elif isinstance (attr, bool):
      if attr:
        val = 1
      else:
        val = 0
    else:
      val = attr
    # print '{0}={1}'.format (name, val)
    msg.append (val)

class Config:
  '''
  class for configuration variables, from config file and command line
  one instance of this object will be used by all the other classes
  '''

  def __init__ (self):
    self.file_list = ['osc_satellite.cfg', os.path.expanduser ('~/.config/osc_satellite.cfg')]

  def set_variables (self):
    '''
    set all the configurable options.  command line overrides settings from file(s)
    '''
    op = self._cmdline ()
    self._set_config_file (op)
    self._load_files ()
    self._load_cmdline (op)

  def _cmdline (self):
    op = optparse.OptionParser (description = 'OSCSatellite.py - generate OSC data from earth satellites')
    op.add_option ('-c', '--configfile',
      dest = 'config_file',
      help = 'optional additional config file')
    op.add_option ('-f', '--tlefilename',
      dest = 'tle_file_name',
      help = 'alternate file for storing TLEs')
    op.add_option ('-a', '--autoupdate',
      dest = 'auto_update',
      help = 'turn auto update for TLEs on',
      action = 'store_true')
    op.add_option ('-n', '--noautoupdate',
      dest = 'auto_update',
      help = 'turn auto update for TLEs on',
      action = 'store_false')

    op.set_defaults (tle_file_name = None)
    op.set_defaults (auto_update = None)

    return op

  def _set_config_file (self, op):
    (opts, args) = op.parse_args ()
    self.config_file = opts.config_file

  def _load_cmdline (self, op):
    '''
    get the remaining command line args and override settings
    from config file(s)
    '''
    (opts, args) = op.parse_args ()
    if opts.tle_file_name is not None:
      self.tle_file_name = opts.tle_file_name
    if opts.auto_update is not None:
      self.auto_update = opts.auto_update

  def dump (self):
    print('\nOSCSatellite.py overview of configuration variables and their settings\n\n')
    for item in dir (self):
        attr = getattr (self, item)
        if isinstance (attr, list):
          print('{0}: '.format (item)),
          for i in attr:
            print(i),
          print()
        elif isinstance (attr, str):
          if item == '__doc__' or item == '__module__':
            continue
          else:
            print('{0}: {1}'.format (item, attr))
        elif (
          isinstance (attr, bool) or
          isinstance (attr, int) or
          isinstance (attr, float)
          ):
          print('{0}: {1}'.format (item, attr))
        else:
          continue

  def _load_files (self):
    '''
    read settings from config file(s)
    user file takes precedence over global file
    leave variables set by command line untouched
    '''

    # tuple of float variables (section, name)
    floats = (('global', 'resolution'),)

    # tuple of integer variables (section, name)
    integers = (
      ('global', 'port'),
      ('global', 'update_interval'),
      ('observer', 'altitude'),)

    # tuple of boolean variables (section, name)
    booleans = (('global', 'auto_update'),)

    # tuple of string variables (secrion, name)
    strings = (
      ('global', 'address'),
      ('global', 'tle_file_name'),
      ('observer', 'longitude'),
      ('observer', 'latitude'),)

    # tuple of string varibales (section, name, type)
    lists = (
      ('global', 'limit_to_satellites', int),
      ('global', 'tle_uris', str),
      ('global', 'exclude_satellites', int),)
 
    # expand list of config files if a filename was specified on command line
    if self.config_file:
      self.file_list.append (self.config_file)

    config = cp.ConfigParser ()
    config_file_list = config.read (self.file_list)
    if len (config_file_list) == 0:
      print('Error:  no config file(s) found')
      sys.exit (1)
    
    try:
      for option in booleans:
        setattr (self, option[1], config.getboolean (option[0], option[1]))

      for option in floats:
        setattr (self, option[1], config.getfloat (option[0], option[1]))

      for option in integers:
        setattr (self, option[1], config.getint (option[0], option[1]))

      for option in strings:
        setattr (self, option[1], config.get (option[0], option[1]))

      for option in lists:
        option_list = []
        for item in config.get (option[0], option[1]).split ():
          option_list.append (option[2] (item))
        setattr (self, option[1], option_list)

      # data has to be treated differently ...
      self.data = []
      for a, b in config.items ('data'):
        self.data.append ((a, b))
  
    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError) as e:
      print('Error reading config file(s): {0}'.format (e))
      sys.exit (3)

class TLEs:
  '''
  '''

  def __init__ (self, config):
    self.config = config
    self.get_from_network ()
    self.load_from_file ()

  def load_from_file (self):
    '''
    create a list of satellites from tle_file_name
    '''

    self.tles = []
    tle = []
    line_number = 0
    
    try:
      tle_file = open (self.config.tle_file_name, 'r')
    except IOError as e:
      print('Error: {0}'.format (e))
      sys.exit (1)

    for line in tle_file.readlines ():
      tle.append (line.strip ())
      line_number += 1
      if line_number == 3:
        self.tles.append ([tle[2][2:7], tle])
        tle = []
        line_number = 0
    print('{0}: loaded {1} TLEs from {2}'.format (time.strftime ('%c'), len (self.tles), self.config.tle_file_name))

  def get_from_network (self):
    '''
    get fresh tle data from specified location
    '''

    if not self.config.auto_update:
      return True

    print('{0}: Updating TLE data:'.format (time.strftime ('%c')))
    try:
      tle_file = open (self.config.tle_file_name, 'w')
    except IOError as e:
      print('Error opening to "{0}": {1}'.format (self.config.tle_file_name, e))
      return False
  
    for uri in self.config.tle_uris:
      try:
        tle = urlhandler.urlopen (uri)
        tle_file.write (str(tle.read ()))
        tle.close ()
        print('Fetched {0}'.format (uri))
      except IOError as e:
        print('Error fetching "{0}": {1}'.format (uri, e))
        tle_file.close ()
        return False
  
    tle_file.close ()
    return True
      
'''
pyephem doc: http://rhodesmill.org/pyephem/quick.html
'''

if __name__ == '__main__':
  '''
  main porgram OSCSatellite
  '''

  # create an instance of class Config ()
  config = Config ()

  # load variables from config file(s)
  config.set_variables ()

  # set signal handlers
  signal.signal (signal.SIGINT, sigint_handler)
  signal.signal (signal.SIGHUP, sighup_handler)
  signal.signal (signal.SIGALRM, sigalrm_handler)
  signal.alarm (config.update_interval * 60 * 60)

  # create an instance of of class TLEs ()
  tles = TLEs (config)

  # create an instance of class Server and start it
  server = Server ()
  server.start ()

  print('\nThis is OSCSatellite.py\n')
  print('Currently using data from {0} satellite(s).'.format (len (server.satellites)))
  print('Observing from longitude={0}, latitude={1}, altitude={2}.'.format (config.longitude, config.latitude, config.altitude))
  if config.auto_update:
    print('Satellite data will be updated every {0} hour(s),'.format (config.update_interval))
    print('using data from:')
    for uri in config.tle_uris:
      print('    {0}'.format (uri))
  else:
    print('No auto update.  Set auto_update = True to change.')
  print('\nUsing IP {0} at port {1}'.format (config.address, config.port))
  print('Send OSC message "/subscribe address:port" to subscribe.')
  print('\nHit Ctrl+C to exit')
  print('Send signal HUP (1) to process id {0} to reload config file.'.format (os.getpid ()))
  print('\nMake good sound and/or imagery!\n\n')

  server.run ()

# vim: tw=0 ts=2
# EOF
