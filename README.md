# OSCSatellite

This is a piece of software that combines the functionality of [pyOSC] and
[pyephem].  It takes the positional data from a list of satellites and
produces OSC messages.

It has so far only been tested on Linux.  Any reports of successfully
installing/running it on different platforms would be really nice (I also
accept problem reports of course).

I distribute my own version of [pyOSC] here for two reasons:

1. The github project has received no updates since June 2011
2. My personal version works for python2 and python3

This version of OSCSatellite has been tested using python 2.7 or python 3.5.

## Installation/Dependencies

Aside from some modules that are part of the standard python
distribution (at least on most systems I have seen), OSCSatellite
depends on two modules:

- [pyOSC] by artm
- [pyephem] by Brandon Craig Rhodes

## Running it

Once you have installed the prerequisites you should take a look at
the configuration file.  A sample is included here
(default_osc_satellite.cfg).  Adjust the parameters to fit your needs
(especially setting up correct location in the [observer] section of
the file).

When done, simply execute (in a terminal window):

  `python OSCSatellite.py -c name_of_config.cfg`

You should see something like this:

```
    Sun Jun  3 13:40:42 2012: loaded 36 TLEs from tle.txt
    Sun Jun  3 13:40:42 2012: Loaded 3 satellite(s)
    
    This is OSCSatellite.py
    
    Currently using data from 3 satellite(s).
    Observing from longitude=-73.0, latitude=46.0, altitude=76.
    No auto update.  Set auto_update = True to change.
    
    Using IP 0.0.0.0 at port 4242
    Send OSC message "/subscribe address:port" to subscribe.
    
    Hit Ctrl+C to exit
    Send signal HUP (1) to process id 12345 to reload config file.
    
    Make good sound and/or imagery!

```

  You can run 'python OSCSatellite.py -h' to get a short overview of
  available switches, but you can also just look at the code to see what you
  can do on startup.

`jogi, 2018-08-27`

[pyOSC]:https://github.com/ptone/pyosc
[pyephem]:http://rhodesmill.org/pyephem/
